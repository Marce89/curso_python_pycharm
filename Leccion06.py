print('#########################     video 35: Calcular estacion segun el Mes  #########################\n')

mes = int(input('Ingresa el mes del año (1-12): '))
mes_texto = ''
estacion = None
band = None

if 10 <= mes <= 12:
    band = 1
    if mes == 10:
        mes_texto = 'Octubre'
    elif mes == 11:
        mes_texto = 'Noviembre'
    elif mes == 12:
        mes_texto = 'Diciembre'
    estacion = 'Primavera hasta el 21/12'

elif 1 <= mes <= 3:
    band = 1
    if mes == 1:
        mes_texto = 'Enero'
    elif mes == 2:
        mes_texto = 'Febrero'
    elif mes == 3:
        mes_texto = 'Marzo'
    estacion = 'Verano hasta el 20/03'

elif 4 <= mes <= 6:
    band = 1
    if mes == 4:
        mes_texto = 'Abril'
    elif mes == 5:
        mes_texto = 'Mayo'
    elif mes == 6:
        mes_texto = 'Junio'
    estacion = 'Otoño hasta el 20/06'

elif 7 <= mes <= 9:
    band = 1
    if mes == 7:
        mes_texto = 'Julio'
    elif mes == 8:
        mes_texto = 'Agosto'
    elif mes == 9:
        mes_texto = 'Septiembre'
    estacion = 'Invierno hasta el 21/09'

else:
    print('\n\t** El mes ingresado no existe **')

if band != 0:
    print(f'\t ****************************************************************')
    print(f'\t ** En el Mes de {mes_texto} estamos en {estacion} **')
    print(f'\t ****************************************************************')

print('#########################     video 36: Etapas de la Vida  #########################\n')

edad = int(input('Ingresa tu edad: '))
mensaje = None

if 0 < edad <= 10:
    mensaje = 'La infancia es Increible...'
elif 10 < edad <= 20:
    mensaje = 'Muchos cambios y mucho estudio...'
elif 20 < edad <= 30:
    mensaje = 'Comienzan los nuevos desafios...'
else:
    mensaje = 'Etapa de la vida no reconocida...'

print(f'\t--> Tu edad es {edad} años,{mensaje}')

###################################################################################################
'''El objetivo del ejercicio es crear un sistema de calificaciones, como sigue:

El usuario proporcionará un valor entre 0 y 10.

Si está entre 9 y 10: imprimir una A

Si está entre 8 y menor a 9: imprimir una B

Si está entre 7 y menor a 8: imprimir una C

Si está entre 6 y menor a 7: imprimir una D

Si está entre 0 y menor a 6: imprimir una F

cualquier otro valor debe imprimir: Valor desconocido'''

nota = float(input('Ingresa la nota del alumno (0-10): '))
mensaje_nota = None
band = None

if 9 <= nota < 10:
    mensaje_nota = 'A'
elif 8 <= nota < 9:
    mensaje_nota = 'B'
elif 7 <= nota < 8:
    mensaje_nota = 'C'
elif 6 <= nota < 7:
    mensaje_nota = 'D'
elif 0 <= nota < 6:
    mensaje_nota = 'F'
else:
    mensaje_nota = '\n** Valor desconocido **'
    band = 1

if band != 1:
    print(f'\n\t--> La nota del Alumno es {mensaje_nota}')
else:
    print(mensaje_nota)
print('############# Video 56: Argumentos Variables llave-valor (Diccionarios) #############')

'''Para manejar una Funcion que pueda listar una Lista de Terminos 
de un DICCIONARIO vamos a utilizar el doble asterisco (**)'''

# Recordar que el metodo .items() nos devuelve una lista de pares de diccionario (llave, Valor)


def listarTerminos (**terminos):
    for llave, valor in terminos.items():
        print(f'{llave}:{valor}')


listarTerminos(IDE=' Integrated Developement Environment', PK=' Primary Key',Test='Se puede seguir agregando mas elementos al diccionario')
listarTerminos(DBMS=' Database Management System')


print('\n############# Video 57: Distintos tipos de Datos como argumentos en Python #############')

'''Vamos a definir una Funcion que reciba una lista de elementos y que acceda a cada uno de ellos'''

def desplegarNombres (nombres):
    for nombre in nombres:
        print(f'{nombre}')

nombrelista = ['Marce','Guillermo','Juan']

'''Si ingresamos al modo debbuger podremos verificar los Tipos de Datos que se va a iterar en nuestra funcion'''
# Tipo LISTA
desplegarNombres(nombrelista) # Pasamos como argumento nuestra lista de nombres
# Tipo STR
desplegarNombres('Carlos\n') # Recordar que una Cadena es una lista de Caracteres
# Tipo TUPLA - los elementos INT deben estar entre Parentesis
desplegarNombres((10, 8))
# Tambien podemos convertirlo a una Lista - los elementos INT deben estar entre corchetes
desplegarNombres([7,44,9,22])

print('\n############# Video 58: Funciones Recursivas en Python #############\n')

'''Una funcion resursiva es llamar a una funcion en si misma'''

# 5! = 5 * 4 * 3 * 2 * 1
# 5! = 5 * 4 * 3 * 2
# 5! = 5 * 4 * 6
# 5! = 5 * 24
# 5! = 120

def factorial(numero):
    if numero == 1:
        return 1
    else:
        return numero * factorial(numero-1)

ingresa_numero = int(input('\tIngresa un numero por favor: '))
resultado = factorial(ingresa_numero)
print(f'\tEl factorial de {ingresa_numero} es {resultado}')

# todo / Tarea 12

'''Imprimir numeros de 5 a 1 de manera descendente usando funciones recursivas. 
Puede ser cualquier valor positivo, ejemplo, si pasamos el valor de 5, 
debe imprimir: 5 4 3 2 1 Si se pasa el valor de 3, debe imprimir: 3 2 1 
Si se pasan valores negativos no imprime nada'''


def imprimir_numero_recursivo(numero):
    if numero >= 1:
        print(numero,end= '|')
        imprimir_numero_recursivo(numero-1)

ing_valor = int(input('\nIngresar un Valor: '))
imprimir_numero_recursivo(ing_valor)
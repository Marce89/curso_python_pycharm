# ABC = Abstract Base Class
from abc import ABC, abstractmethod

print(' Video 93: Clases Abstractas en Python '.center(70, '#'))

'''
> Basicamente al definir un Metodo como ABSTRACTO toda la Clase pasa a ser abstracta.Esto nos va a permitir que al crear 
Clases HIJAS debamos impletar obligatoriamente el/los METODO/s declarados en la/s clase/s Padre/s 

> Un método abstracto es un método que no tiene una implementación, es decir, que no lleva código. Un método definido 
con este decorador, forzará a las clases que implementen dicho interfaz a codificarlo.

> Un dato a tener en cuenta es que NO se podra instanciar (crear objetos) en nuestra clase padre, ya que al convertirse
en una Clase Abstracta python no permite esta accion y solo se podra instanciar a travez de las Clases Hijas
(NOTA : Para entenderlo mejor comentar todas las clases HIJAS y crear un objeto en la clase PADRE)
'''


class Terreno(ABC):
    def __init__(self, frente, fondo):
        self.__frente = frente
        self.__fondo = fondo

    @property
    def get_frente(self):
        return f'{self.__frente}'

    @property
    def get_fondo(self):
        return f'{self.__fondo}'

    @abstractmethod # Con este decorador estamos definiendo a este metodo como ABSTRACTO,siendo este requerido para TODAS las clases HIJAS
    def area_del_terreno(self):
        pass

class Pileta(Terreno):
    def __init__(self, frente, fondo):
        super().__init__(frente, fondo)

    def area_del_terreno(self):   # Si comentamos este METODO ,python nos advertira que debemos impletar el metodo ya que es requerido pora la ejecucion de la clase HIJA
        return f'\nEl area necesaria para instalar la Pileta es de {int(self.get_frente) * int(self.get_fondo)} m2'

class Quincho(Terreno):
    def __init__(self, frente, fondo):
        super().__init__(frente,fondo)

    def area_del_terreno(self):
        return f'\nEl area necesaria para hacer el Quincho es de {int(self.get_frente) * int(self.get_fondo)} m2'


if __name__ == '__main__':
 pileta = Pileta(11, 4)
 print(pileta.area_del_terreno())

 quincho = Quincho(10, 6)
 print(quincho.area_del_terreno())


 # objeto_instaciando_clase_padre = Terreno(2, 3)
 # print(objeto_instaciando_clase_padre.area_del_terreno())
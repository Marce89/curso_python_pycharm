print(' Video 94: Variables de Clase en Python '.center(60, '#'))


class MiClase:
    # Esta variable se va a compartir con todos los OBJETOS
    variable_clase = 'valor variable de clase'

    def __init__(self, variable_instancia):
        # Esta variable dependerà de c/u de los OBJETOS,ya que podremos pasarle diferentes valores
        self.variable_instancia = variable_instancia


print(f'\nAccedo directamente desde la Clase: {MiClase.variable_clase}')

obj_1 = MiClase('variable de Instancia')
print(f'\nObj_1 --> Accedo a la variable de Instancia: {obj_1.variable_instancia}')
obj_2 = MiClase('Otro valor de instancia')
print(f'Obj_2 --> Accedo a la variable de Instancia: {obj_2.variable_instancia}')

# Podemos acceder a la Variable de Clase desde nuestros OBJETOS

print(f'\nAccedo a la variable clase desde el Objeto: {obj_2.variable_clase}\n')

print(' Video 95: Creacion de variables de Clase al vuelo '.center(60, '#'))
''' Podemos crear una variable de clase al vuelo (no definido en la plantilla >Class MiClase<)'''

MiClase.variable_clase_al_vuelo = 'TEST_QA_AUTOMATION'
print(f'\nAccediendo a travez del obj_2 --> {obj_2.variable_clase_al_vuelo}\n')  # El IDE no autocompleta esta variable ya que fue creado fuera de la clase (plantilla)

print(' Video 96 y 97 : Metodos Estàticos y Metodos de Clases en Python '.center(100, '#'))

'''
> Este decorador hara que el METODO se asocie con la CLASE misma y no con los OBJETOS
> A travez de este metodo solo podremos acceder a la/s varible/s de CLASE de manera Indirecta
> Un METODO DE CLASE si recibe un contexto de CLASE (recibe informacion a diferencia de los Metodos estàticos)
'''

####################### Contexto Estàtico ##################

class Moto:                                              #
    def __init__(self, cc, marca):
        self.cc = cc
        self.marca = marca

    variable_clase_color = 'ROJO'

    @staticmethod
    # La 1er diferencia que se obeserva es que el METODO ya no recibe el parametro SELF,
    # ya que un metodo estatico no puede acceder a los variables de INSTANCIA
    def metodo_estatico():   # NO TIENE self
        # ¿Por que los metodos de Clases y Estàticos NO pueden ser retornados (return) ?

        print(f'\nAccedo a travez del CONTEXTO Dinamico -> V.C = {Moto.variable_clase_color}')

    @classmethod
    def metodo_de_clase(cls):  # Automaticamente recibe el parametro de CLS,este funciona como el SELF pero solo para los metodos de clase
        print(f'Accedo a la variable de clase a travez del Contexto Estatico : {cls.variable_clase_color}')  # accedemos a la varible_de_clase a tarvez del parametro >cls<

    def metodo_instancia(self):
        print(' Accedo a travez del metodo_instacia '.center(65, '*'))
        self.metodo_de_clase()
        self.metodo_estatico()
        print(self.variable_clase_color)

##################### Contexto Dinàmico ###################
                                                         #
Moto.metodo_estatico()
obj_moto1 = Moto(1300, 'Yamaha')
print(f'\nCilindrada: {obj_moto1.cc} - Marca:{obj_moto1.marca}\n')

Moto.metodo_de_clase()

''''
El Contexto Dinamico PUEDE acceder al Contexto Estatico, en cambio el Contexto Estatico NO puede acceder al Contexto Dinamico 
'''
# Ej:
obj_moto2 = Moto(400, ' Imperiale')
obj_moto2.metodo_instancia()

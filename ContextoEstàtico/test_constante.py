from constante import  MI_CONSTANTE, Matematicas as mate

'''
Utilizando < as > podemos renombrar en este caso la Clase 
(tambien podriamos renombrar la variable MI_CONSTANTE si asi lo quisieramos) 
'''
print(MI_CONSTANTE)
print(F'Variable de Clase: {mate.PI}...')

'''
Si bien python NOS permite modificar el valor de las Constantes,
no debemos modificarlos ya que no es buena practica
'''
# MI_CONSTANTE = 'Modifico el valor'
# print(MI_CONSTANTE)

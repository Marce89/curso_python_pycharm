print(' Video 99 y 100: Ejercicio Contador Objetos '.center(65, '#'))


class Persona:
    contador_persona = 0  # Iniciamos el contador en cero

    @classmethod
    def generar_siguiente_valor(cls):
        cls.contador_persona += 1
        return cls.contador_persona

    def __init__(self, nombre, edad):

        self.id_persona = Persona.generar_siguiente_valor() #Llamo al metodo de clase
        self.nombre = nombre
        self.edad = edad

    def __str__(self):
        print('--------------------------------------------')
        return f'Persona: id-> {self.id_persona} |\t{self.nombre} |\t{self.edad} años |'


persona1 = Persona('Mariano', 23)
print(persona1)
persona2 = Persona('Raul', 48)
print(persona2)
persona3 = Persona('Julieta', 36)
print(persona3)
print(f'\nValor del contador = {Persona.contador_persona}')
'''
Debemos tener en cuenta que cada vez que llamemos al metodo de clase estaremos 
aumentando el valor de nuestro contador en 1 , y eso puede no ser no requerido 

>>>> Ver las siguientes lineas de codigo
'''
Persona.generar_siguiente_valor()  # Aca el id = 4
persona4 = Persona('Sofia', 55)    # Aca el id = 5
print(persona4)

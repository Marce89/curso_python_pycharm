print(' Video 98: Constantes en Python '.center(65,'#'))
MI_CONSTANTE = 'es una constante'

'''
1. Por convencion en python se define a las variables constantes con Mayusculas,
para que el desarrollador sepa que la variable NO debe ser modificada
2. Es buena practica que las Variables Constantes se encuentren definidas en un modulo (archivo)
'''

class Matematicas:
    PI = 3.1416

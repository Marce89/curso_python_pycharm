print('#########################     video 38  #########################\n')

'''Ciclo while se ejecutara tantas veces como ls condicion sea True, finalizando
si y solo si la condicion es False'''

# condicion = False
#
# while condicion:
#     print('Se ejecuta infinitas veces por que la condicion es TRUE')
# else:
#     print('Fin del Ciclo ,la condicion es FALSE')

# contador = 0
#
# while contador < 3:
#     print(f'{contador+1}º)Ejecucion del programa')
#     contador += 1  # contador = contador +1
# else:
#     print('\nFin del Ciclo ,ya que no cumple con la condicion')

#print('###############     Imprimir los números naturales del 0 al 10    ###############')

# num = 0
#
# while num < 10:
#     print(f'Se imprime el numero {num + 1}')
#     num += 1
# else:
#     pass


print('###############     Imprimir numeros de 5 a 1 de manera descendente    ###############')

i = 5
while 1<=i<=5:  #Tambien prodia haber preguntado while (i != 0)
    print(i)
    i -= 1
else:
    pass
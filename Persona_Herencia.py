if __name__ == '__main__':   # uso este metodo para poder importar las CLASES a otro modulo sin que se ejecute el codigo fuera de las CLASES

    print(' Video 82: Ejemplo de Herencia en Python '.center(70, '#'))

'''
>>> super().__init__(atributo1_clase_padre, atributo1_clase_padre, ....) sin este CONSTRUCTOR no podremos ACCEDER a los atributos de la clase PADRE
cuando estemos creando objetos a partir de nuestras clases HIJAS, ya que sin el NO esta Herendando dichos atributos

'''


class Persona:

    def __init__(self, nombre, edad):
        self.__nombre = nombre
        self.__edad = edad

    def __str__(self):
        return f'clase Persona -->(Nombre:{self.__nombre} | Edad: {self.__edad} años)'

    @property
    def getnombre(self):
        return self.__nombre

    @getnombre.setter
    def set_nombre(self, nom):
        self.__nombre = nom

    @property
    def getedad(self):
        return self.__edad


class Empleado(Persona):

    def __init__(self, nombre, edad, sueldo, legajo):  # 1)Es necesario INICIALIZAR los atributos de la Clase PADRE mas los Valores que queramos agregar a esta nueva clase HIJA ej: < sueldo >
        super().__init__(nombre, edad)      # 2)Mandamos a llamar al constructor de la Clase padre
        self.sueldo = sueldo                # 3)Creamos los demas atributos de la clase HIJA
        self.legajo = legajo

    # Para REUTILIZAR codigo podemos llamar al metodo __str__ con la funcion -super- (ya que nos permite ingresar a los atributos y metodos del PADRE)

    def __str__(self):
        return f'clase Empleado -->(Sueldo: {self.sueldo} | Legajo: {self.legajo}) {super().__str__()}'

    def mostrar_detalle(self):
        print(f'\nNombre: {self.getnombre}\nEdad: {self.getedad} años\nSueldo: {self.sueldo} $\nLegajo: {self.legajo}')


'''
Notemos que dentro de nuestro Metodo <mostrar_detalle> accedemos a los atributos PRIVADOS __nombre y __edad a travez
del los metodos <getNombre> y <getEdad> respectivamente (sin los parentesis gracias a los DECORADORES) 
'''


class Puesto(Empleado):
    def __init__(self, nombre, edad, sueldo, legajo, puesto):
        super().__init__(nombre, edad, sueldo, legajo)
        self.__puesto = puesto

    @property
    def met_get_puesto(self):
        return self.__puesto

    def mostrar_detalle(self):
        print(f'\nNombre: {self.getnombre}\nEdad: {self.getedad} años\nSueldo: {self.sueldo} $\nLegajo: {self.legajo}\nPuesto: {self.met_get_puesto}')


if __name__ == '__main__':
    empleado1 = Empleado('Fernando', 31, 50000, 2485)
    # empleado1.set_nombre = 'Marcos'
    print(empleado1.mostrar_detalle())
    empleado2 = Empleado('Martin', 45, 154785, 1452)
    print(empleado2.mostrar_detalle())
    empleado3 = Puesto('Leonardo', 25, 26060, 741, 'Operario')
    print(empleado3.mostrar_detalle())
    empleado4 = Puesto('Marina', 28, 60500, 853, 'Administrativa')
    print(empleado4.mostrar_detalle())

print('#########################     video 23  y 24    #########################\n')


class Ejercicios:
    def __init__(self ,dato):
        self.dato = dato


    def ejer_par_impar_v23(self):
        if self.dato % 2 == 0:
            print(f'El numero {self.dato} es PAR')
        else:
            print(f'El numero {self.dato} es IMPAR')

    def ejer_edad_adulto(self):
        mayoredad = 18
        if self.dato >= mayoredad:
            print(f'La persona ya es un Adulto, puede votar')
        else:
            print(f'Todavia es menor de edad !!')


#entrada_dato = int(input('Ingresa un numero: '))
entrada_dato = int(input('Ingresa la Edad: '))

obj=Ejercicios(entrada_dato)

#obj.ejer_par_impar_v23()
obj.ejer_edad_adulto()

'''Lo que hice son 2 ejercicios en una misma clase, para que ejecutar de a una 
funcion -comento- la entrada y la instancia'''
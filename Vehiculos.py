
class Camiones:
    def __init__(self, Marca, Modelo , Potencia):
        self.marca = Marca
        self.modelo = Modelo
        self.potencia = Potencia

    def mostrar_datos(self):
        print(f'\n\tVehiculo: {self.marca}\n\tModelo: {self.modelo}\n\tPotencia: {self.potencia} hp')


class Deportivos:
    def __init__(self, Marca, Modelo , Potencia , velocidad):
        self.marca = Marca
        self.modelo = Modelo
        self.potencia = Potencia
        self.velocidad = velocidad

    def mostrar_datos(self):
        print(f'\n\tVehiculo: {self.marca}\n\tModelo: {self.modelo}\n\tPotencia: {self.potencia} hp\n\tVelocidad Max.:{self.velocidad} km/h')

    #Agregamos el Metodo DESTRUCTOR en nuestra clase
    def __del__(self):
        print(f'\nSe destruye el obj_deportivo: {self.marca} {self.modelo}')


'''
############# Video 79: Comprobacion del Modulo Main (Principal) en Python #############

** Este metodo nos permite que todo el codigo que se encuentre FUERA de nuestra/s CLASE/S NO se ejecute cuando Importamos a otro Modulo (archivo)

** Cuando el nombre del archivo que se esta ejecutando es el mismo,al imprimir la propiedad __name__ nos devolvera la cadena '__main__'

** Con el metodo <if __name__ == '__main__':> 

'''

# Comentar el METODO if para entender la propiedad __name__
#if __name__ == '__main__':
print('\n>>> Se muestra todo el codigo que se encuentre fuera de la/s clase/s en el modulo "VehiculosModulos.py" <<<\n')
print(f'''Todo el codigo de arriba pertenece al modulo {__name__}.py  ''')

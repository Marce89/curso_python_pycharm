print('#########################     Video 21   #######################\n')

'''Operadores de Asignacion en Python '''


class Asignacion:
    def __init__(self,ingresodato):
        self.ingresodarto = ingresodato

    def aumenta_valor(ingresodato):
        q = 1
        ingresodato += q
        return print(f'\t*Ingresaste {pr1} ,si le aumentamos {q} entonces nos da {ingresodato}')

    def disminuir_valor(ingresodato):
        q = 5
        ingresodato -= q
        return print(f'\t*Ingresaste {pr1} ,si le disminuimos {q} entonces nos da {ingresodato}')

    def multiplicar_valor(ingresodato):
        q = 10
        ingresodato *= q
        return print(f'\t*Ingresaste {pr1} ,si multiplicamos por {q} entonces nos da {ingresodato}')

    def dividir_valor(ingresodato):
        q = 2
        ingresodato /= q
        return print(f'\t*Ingresaste {pr1} ,si dividimos por {q} entonces nos da {ingresodato}')


pr1=int(input(f'Ingresa un valor: '))

objeto = Asignacion.aumenta_valor(pr1)
objeto = Asignacion.disminuir_valor(pr1)
objeto = Asignacion.multiplicar_valor(pr1)
objeto = Asignacion.dividir_valor(pr1)

print('\n#########################     Video 22   #######################')

'''Operadores de Comparacion en python'''


class Comparando:

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def igual(self):
        resultado = self.a == self.b
        print(f'\n¿Es {self.a} IGUAL que {self.b}?')
        if resultado:
            texto = 'es Igual'
        else:
            texto = 'no es Igual'
        return print(f'\t*Esta condicion es {resultado} ya que {self.a} {texto} que {self.b} ')

    def distinto(self):
        resultado = self.a != self.b
        print(f'\n¿Es {self.a} DISTINTO que {self.b}?')
        if resultado:
            texto = 'es Distinto'
        else:
            texto = 'no es Distinto'
        return print(f'\t*Esta condicion es {resultado} ya que {self.a} {texto} a {self.b} ')

    def mayor(self):
        resultado = self.a > self.b
        print(f'\n¿Es {self.a} MAYOR que {self.b}?')
        if resultado:
            texto = 'es Mayor'
        else:
            texto = 'no es Mayor'
        return print(f'\t*Esta condicion es {resultado} ya que {self.a} {texto} que {self.b} ')

    def mayor_igual(self):
        resultado = self.a >= self.b
        print(f'\n¿Es {self.a} MAYOR o IGUAL que {self.b}?')
        if resultado:
            texto = 'es Mayor Igual'
        else:
            texto = 'no es Mayor ni Igual'
        return print(f'\t*Esta condicion es {resultado} ya que {self.a} {texto} a {self.b} ')

    def menor(self):
        resultado = self.a < self.b
        print(f'\n¿Es {self.a} MENOR que {self.b}?')
        if resultado:
            texto = 'es Menor'
        else:
            texto = 'no es Menor'
        return print(f'\t*Esta condicion es {resultado} ya que {self.a} {texto} que {self.b} ')

    def menor_igual(self):
        resultado = self.a <= self.b
        print(f'\n¿Es {self.a} MENOR o IGUAL que {self.b}?')
        if resultado:
            texto = 'es Menor Igual'
        else:
            texto = 'no es Menor ni Igual'
        return print(f'\t*Esta condicion es {resultado} ya que {self.a} {texto} a {self.b} ')


a_dato = int(input('Ingresa un valor: '))
b_dato = int(input('Ahora ingresa nuevamente un valor: '))

objeto = Comparando(a_dato, b_dato)

objeto.igual()
objeto.distinto()
objeto.mayor()
objeto.mayor_igual()
objeto.menor()
objeto.menor_igual()
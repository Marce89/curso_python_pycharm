print('#########################     video 31 y 32  #########################\n')


condicion = 'hola'

if condicion == True:
    print(f'Se ejecuta todo el codigo cuando la condicion es TRUE o sea IF')
elif condicion == False:
    print(f'Se ejecuta todo el codigo cuando la condicion es FALSE o sea ELIF')
else:
    print(f'Se ejecuta todo el codigo cuando no es True ni False o sea ELSE')


'''Con el modo debug podemos ejecutar paso a paso nuestro codigo,se debe marcar
 Presionando F8 podremos ir ejecutando cada paso'''

print('#########################     video 33: Ejercicio Conversion de numero a texto  #########################\n')

numero = int(input('Ingresa un numero entre 1 y 4: '))

numerotexto = ''

if numero == 1:
    numerotexto = 'Numero uno'
elif numero == 2:
    numerotexto = 'Numero dos'
elif numero == 3:
    numerotexto = 'Numero tres'
elif numero == 4:
    numerotexto = 'Numero cuatro'
else:
    numerotexto = 'Valor fuera de rango'

print(f'Ingresaste el {numero} - {numerotexto}')

print('#########################     video 34: Sintaxis if-else simplificada (Operador ternario)   ####################\n')

condicion = True

# if condicion:
#     print(f'Condicion VERDADERA')
# else:
#     print(f'Condicion FALSA')

''''
CONTRA: 
    * No es recomendable usarse en grandes codigos
    * No se pued usar ELIF
PRO:
    * Sirve para simplificar condigos pequeños en una sola linea
'''

print('Ejecuta si la condicion es TRUE') if condicion else print('Ejecuta si la Condicion es FALSE')

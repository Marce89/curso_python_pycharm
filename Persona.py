print('\n############# Video 59: Clases y Objetos en Python Parte I #############\n')

'''
+ Una CLASE es una Plantilla, esta posee un nombre,atributos y metodos(funciones)
+ Un OBJETO es una instancia de una Clase
'''


class Persona:
    pass


print(type(Persona))

print('\n############# Video 60: Clases y Objetos en Python Parte II #############\n')


class Persona1:
    # mi_atributo= ' ' son Atributos de la CLASE simepre que se encuentren antes de los metodos
    def __init__(self):
        self.nombre = 'JUAN'     # Estos son Atributos de los OBJETOS
        self.apellido = 'SUAREZ'
        self.edad = 35    #No es comun asignar valores por default a los atributos, luego se vera como hacerlo con parametros del metodo __init__


People1 = Persona1()  # Aca creamos el OBJETO
print(f'Objeto de la clase Persona1:\n\tNombre: {People1.nombre}\n\tApellido: {People1.apellido}\n\tEdad: {People1.edad} años')

print('\n############# Video 61: Creacion de Objetos con Argumentos #############\n')


class Persona2:
    def __init__(self, nombre, apellido, edad):  # Estos son los 3 PARAMETROS que recibira este METODO
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad

People2 = Persona2('Marcelo Ezequiel','Figueroa',32)
print(f'Objeto_persona2 de la clase Persona2:\n\tNombre: {People2.nombre}\n\tApellido: {People2.apellido}\n\tEdad: {People2.edad} años')


print('\n############# Video 62: Creacion de mas Objetos de una Clase #############\n')

'''Basicamente lo esta mos haciendo es crear otro OBJETO de la misma CLASE -Persona2-'''

People3 = Persona2('Fernando Luis','Figueroa',32)
print(f'Objeto_persona3 de la clase Persona2:\n\tNombre: {People3.nombre}\n\tApellido: {People3.apellido}\n\tEdad: {People3.edad} años')

print('\n############# Video 63: Referencia de Memoria de Objetos y Ejecucion Paso a Paso #############\n')

print('''En el video se puede visualizar en el modo dubbuger como los Objetos apuntan
            cada uno a una direccion en memoria DIFERENTE

            Class A --> Obj1  --> direccion memoria  x...20

            Class B --> Obj2  --> direccion memoria  x...31
            Class B --> Obj3  --> direccion memoria  x...85
            ''')
print('\n############# Video 64: Modificar Atributos de un Objeto #############\n')

'''Vamos a modificar los atributos de People1 (Nuestro 1º OBJETO) '''

People1.nombre = 'Sandra'
People1.apellido = 'Gomez'
People1.edad = 25
print(f'Modifico los atributos del Objeto :\n\tNombre: {People1.nombre}\n\tApellido: {People1.apellido}\n\tEdad: {People1.edad} años')

print('\n############# Video 65: Metodos de Instancia en Python #############\n')

'''
1)  Una Clase dijimos que contiene ATRIBUTOS pero tambien posee METODOS,siendo los Atributos,las caracteristicas que van a tener
    nuestros OBJETOS al momento de crearlo a traves de nuestra clase , en cambio el/los Metodos va a ser el COMPORTAMIENTO que van a tener
    nuestros Objetos

2)  Si estamos DENTRO de nuestra Clase,en la definicion de la misma,podemos acceder a los Atributos a traves del Parmetro SELF. (EJ1)
    Ahora si queremos acceder a los atributos FUERA de la definicion de nuestra Clase solo se podra acceder creando una variable que este apuntando
    a nuestro Objeto (EJ2)
'''


class Auto:
    def __init__(self,marca,color,hp):
        self.marca = marca
        self.color = color
        self.hp = hp

    def mostrar_detalles(self): # El parametro de SELF se agrega automaticamente, y este se agrega a todos los 'Metodos de Instancia' (Estos se conocen asi ya que que se van a asociar con los Objetos que vamos a crear)
                                # Podemos agregar mas PARAMETROS propios del metodo si asi lo necesitaramos,  def mostrar_detalles(self, parametro1, parametro2 , ...)

        # EJ1: Acceso de Atributos de Clase a partir del Parametro SELF
        print(f'''Acceso de Atributos de Clase a partir del Parametro SELF
        \n\tVehiculo: {self.marca}\n\tColor: {self.color}\n\tPotencia: {self.hp} hp
        ----------------------------------''')


car1=Auto('Ferrari','Rojo',340)
car1.mostrar_detalles()

car2=Auto('Dodge','Negro',240)
# EJ2: Acceso de Atributos de Clase a partir del Objeto
print(f'''Acceso de Atributos de Clase a partir del Objeto
\nVehiculo:{car2.marca}\nColor:{car2.color}\nPotencia:{car2.hp}
''')

print('\n############# Video 66: Mas de SELF y Atributos de Instancia en python #############\n')

'''
1) EJ1: La variable SELF no es obligatorio que se llame asi,se puede reemplazar por cualquier nombre
2) EJ2: Podemos repetir el nombre de los Metodos siempre y cuando no sea de la misma CLASE
3) EJ3: Asi tambien podemos repetir el nombre de los Objetos apuntando a una direccion en memoria diferente,aunque esto NO es una buena practica
4) EJ4: En cualquier momento podemos agregar ATRIBUTOS a un Objeto,solo se debe tener en cuenta que esto no se compartira con los demas Objetos
'''

class Camion:
    def __init__(this,marca,color,hp):  # EJ1
        this.marca = marca
        this.color = color
        this.hp = hp

    def mostrar_detalles(this):      # EJ2 : igual que la CLASE -Auto-
        print(f'\n\tVehiculo: {this.marca}\n\tColor: {this.color}\n\tPotencia: {this.hp} hp')

# EJ3
Car2=Camion('Mercedes Benz','Azul',3400)
Car2.mostrar_detalles()
# EJ4: Agrego un Atributo a un Objeto
Car2.ejes = '3+1'
Car2.largo = 22.4
print(f'\tCant. de ejes: {Car2.ejes}\n\tLargo: {Car2.largo} mts')

# EJ4: Los atributos -ejes- y -largo- NO se muestran para este Objeto (arrojara error en la Linea 135)
'''Comentar las lineas 133,134 y 135 para ver el codigo del video 73 '''
Car3=Camion('Volvo','Blanco',2100)
Car3.mostrar_detalles()
print(Car3.ejes,Car3.largo)

print('\n############# Video 73: Robustecimiento del Metodo Init #############\n')

'''
*args: Son argumentos variable de tipo Tupla
**kwargs: Son argumentos variables llave-valor de tipo Diccionario
Estos pueden cambiarse los nombres, lo que importa es el/doble asterisco

$$  Tener en cuenta que Primero se pasaran la TUPLAS y despues los DICCIONARIOS  $$ 
'''

class Persona3:
    def __init__(self, nombre, apellido, edad , *args, **kwargs):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
        self.arg = args
        self.kwargs = kwargs
    def mostrarDetalles(self):
        print(f'{self.nombre} {self.apellido} {self.edad} {self.arg} {self.kwargs}')

objPeople1 = Persona3('Marcelo Ezequiel','Figueroa',32,'testing',4,'automation',3.2,m='marte',tc='Teoria de las Cuerdas')
objPeople1.mostrarDetalles()

objPeople2 = Persona3('Pedro','Gomez',55)
objPeople2.mostrarDetalles()
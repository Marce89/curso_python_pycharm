print('#################   Video 42 y 43: listas en Python    ##############')
# Definimos una lista de Tipo str

nombres = ['juan', 'carla', 'Ricardo', 'maria']
# Accedemos a los elementos de nuestra lista
print(nombres[2])
print(nombres[0])
# Podemos traerclos elementos de la lista en un rango que va desde la izq. INCLUIDO y a la derecha NO Incluido
print(nombres[1:3])
# Si queremos incluir a MARIA se debe colocar [1:4]
print(nombres[1:4])
# Acceder al Inicio de nuestra lista al indice (Sin Incluirlo) NO IMPRIME MARIA
print(nombres[:3])
# Acceder desde el indice (Incluido) hasta el final de la lista NO IMPRIME JUAN
print(nombres[1:])

'''Cambiamos el valor de un elemento de la lista'''
nombres[3] = 'esteban'
print(nombres)


'''Iteramos una Lista'''

for i in nombres:
    print(f'{i}')
else:
    print('No hay mas nombres en la Lista..')

print('\n#################   Video 44: listas en Python 3 parte    ##############')

'''¿Como podemos saber el largo/cantidad de elementos de nuestra lista?'''

print(f'\n\t*Esta lista contiene', len(nombres), 'elementos\n')

'''¿Como podemos agregar elementos al final de nuestra lista?'''

nombres.append('marcelo')
print(f'''\n*El elemento se agrega al FINAL de la lista
{nombres}''')

'''¿Como Insertamos un elemento en nuestra lista en un indice especifico?'''

nombres.insert(2, 'matias')
print(f'''\n*El elemento -Matias- tomara la posicion 2 y los demas elementos se moveran hacia la derecha cambiando su indice
{nombres}''')

'''¿Como eliminamos un elemento de la lista?'''

nombres.remove('matias')
print(f'''\n*Se elimina el elemento -matias-
{nombres}''')

'''¿Como remover el ultimo de la lista?'''

nombres.pop()
print(f'''\n*Se elimina el elemento -marcelo-
{nombres}''')

'''¿Como eliminamos un INDICE de la lista?'''

del nombres[0]
print(f'''\n*Se elimina el elemento -juan- de la lista
{nombres}''')

'''¿Como LIMPIAR una lista?'''

nombres.clear()
print(f'''\n*Lista vacia
{nombres}''')

'''¿Como ELIMINAMOS un a Lista?'''

del nombres
print(nombres)


print('\t········         TAREA       ...............')
lista_de_numeros = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for numero in lista_de_numeros:
    resto = numero % 3
    if resto == 0:
        print(f'El {numero} es divisible por 3')

# Otra forma mas simple seria

for i in range(0,10):
    resto=i % 3
    if resto == 0:
        print(i)

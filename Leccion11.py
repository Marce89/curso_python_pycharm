print('############# Video 50: Funciones en Python #############')

'''Una funcion nos permite reutilizar codigo en cual parte de nuestro programa ya que podemos
mandarlo a llamar desde cualquier modulo/archivo '''

# todo/ Notacion Camello
'''Supongamos que el nombre de la funcion tiene varias palabras'''


def miFuncionDePython():
    print('''
     Este tipo de notacion 'camello' o 'Altas y bajas'
     se escribe la primer palabra de la funcion en minuscula
     y despues todas las demas comienzan con mayuscula
          ''')
# todo/ Notacion guion bajo _


def mi_funcion_de_python():
     print('''
          Este otro tipo de notacion tambien es valida,
          separando las palabras con guion bajo (_)
          ''')


print(miFuncionDePython())
print(mi_funcion_de_python())

print('############# Video 51: Paso de Argumentos en Python #############')

'''Usando -Debug- luego de ingresar un punto de interrupcion,podemos ingresar al detalle
 de la ejecucion de nuestro programa presionando F7 (Step Into)'''


def mi_funcion(nombre, apellido):
    print(f'''Nombre: {nombre}
Apellido: {apellido}\t''')
    print('-----------')


mi_funcion('Marcelo', 'Figueroa')
mi_funcion('Carlos', 'Sanchez')

print('############# Video 52: Return en Funciones en Python #############')

def sumar (a, b):
    return a + b

resultado = sumar(8, 5.4)
print(f'\nEl resultado de la suma: {resultado}')    # Aca lo llamamos a la VARIABLE -resultado-
print(f'\nEl resultado de la suma: {sumar(2,7)}')   # Podemos llamar directamente a la FUNCION -sumar-
print('############# Video 53: Valores por default de los parametros de una Funcion #############')


# def restar(a:int =0, b:int =0) -> int:    # La ntacion completa seria asi pero no es necesario ya que la variables en python son -Dinamicas*
def restar(a = 0, b = 0):

    return a - b


resultado = restar()
print(f'\nEl resultado de la resta: {resultado}')
resultado = restar(5, 2)
print(f'\nEl resultado de la resta: {resultado}')

print('############# Video 54: Argumentos Variables en Funciones #############')

'''
1) Vamos a anteponer el (*) aserisco en nuestro parametro ya que no sabemos la cantidad de argumentos que vams a recibir
2) Dentro de nuestro codigo esto funcionara como una TUPLA'''

def listarNombres(*nombres):   # Aca se  guardaran los argumentos como un TUPLA
    for nombre in nombres:
        print(nombre,end='/')

listarNombres('Juan','Carla','Maria','Alberto')
listarNombres('Mariano','Veronica','Armando')

'''Crear una función para sumar los valores recibidos de tipo numérico,
utilizando argumentos variables *args como parámetro de la función y 
regresar como resultado la suma de todos los valores pasados como argumentos
'''

def sumarLista(*numeros):
    resultado = 0
    for valor in numeros:
       resultado +=  valor
    return resultado


print(f'La suma total es {sumarLista(14,10,10)}')


'''
Crear una función para multiplicar los valores recibidos de tipo numérico, 
utilizando argumentos variables *args como parámetro de la función y regresar 
como resultado la multiplicación de todos los valores pasados como argumentos.
'''

def multiLista(*numeros):
    resultado = 1
    for valor in numeros:
       resultado *=  valor
    return resultado


print(f'La multiplicacion total es {multiLista(2,2,2,1,4,2,7)}')
from Persona_Herencia import *         # Con el * Importo TODAS las Clases de modulo Persona_Herencia.py

print(' Video 83: Sobreescritura del metodo __str__() en Python '.center(70, '#'))
print('\n')
'''
> ¿Cuando utilizamos __str__()?
  Cuando intentamos acceder a una instancia (OBJETO) directamente ,sin especificar el atributo o metodo (ver lineas 13 y 21)
  
> Si no utilizamos el metodo __str__ al imprimir el objeto solo se mostrara la direccion en memoria 
'''

persona1 = Persona('Bastian Schweinsteiger', 37)
print(persona1)

'''
Para poder ver el SUELDO y LEGAJO del empleado ,necesitamos SOBREESCRIBIR tambien el metodo __str__
en la Clase HIJA <Empleado> ,ya que de lo contrario solo se mostraran el NOMBRE y EDAD heredados de la
clase PADRE <Persona>
'''
empleado4 = Empleado('Martin', 36, 87000, 96851)
print(empleado4)

print('\n')
print(' Tarea 13: Ejercicio de Herencia en Python '.center(70, '#'))
'''Definir una clase padre llamada Vehiculo y dos clases hijas llamadas Coche y Bicicleta, 
las cuales heredan de la clase Padre Vehiculo.La clase padre debe tener los siguientes atributos 
y métodos:

Vehiculo (Clase Padre):
    -Atributos (color, ruedas)
 -Métodos ( __init__() y __str__ )

 Coche  (Clase Hija de Vehículo) (Además de los atributos y métodos heradados de Vehículo):
    -Atributos ( velocidad (km/hr) )
 -Métodos ( __init__() y __str__() )

 Bicicleta  (Clase Hija de Vehículo) (Además de los atributos y métodos heradados de Vehículo):
    -Atributos ( tipo (urbana/montaña/etc )
 -Métodos ( __init__() y __str__() )'''


class Vehiculo:
    def __init__(self, color, ruedas):
        self.color = color
        self.ruedas = ruedas

    def __str__(self):
        return f'Color: {self.color}\nCant. ruedas: {self.ruedas}'


class Coche(Vehiculo):
    def __init__(self, color, ruedas, velocidad):
        super().__init__(color, ruedas)
        self.velocidad = velocidad

    def __str__(self):
        return f'{super().__str__()}\nVelocidad: {self.velocidad} km/h\n'


class Bicicleta(Vehiculo):
    def __init__(self, color, ruedas, tipo):
        super().__init__(color, ruedas)
        self.tipo = tipo

    def __str__(self):
        return f'{super().__str__()}\nTipo: {self.tipo}\n'


if __name__ == '__main__':
    bici = Bicicleta('roja', 2, 'MontainBike')
    print(bici)

    auto = Coche('Negro', 4, 210)
    print(auto)

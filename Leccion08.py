print('#################   Video 39: Ciclo For Python    ##############')
cadena = 'Python'

'''El ciclo for lo que hace es Iterar (RECORRER) cada elemento de nuestro array
en este caso es la variable -cadena-'''

for letra in cadena:
    print(letra)
else:
    print('Fin del Ciclo')

numerosyletra = [1,2,3,'PR',7,8,9,'hola']

for i in numerosyletra:
    print(i)

print('#################   Video 40: BreaK     ##############')

'''Con el -break- hacemos que al encontrar el elemento que necesitamos de nuestra
lista,se corte la iteracion del Ciclo ,asi tambien del ELSE'''

# Con el break le decimos al programa Finaliza la ejecucion del codigo

lista = [1, 2, 3, 4, 5, 'Patricio Rey', 6, 7]

for i in lista:
    if i == 'Patricio Rey':
        print(i)
        break
else:
    print('Fin del Ciclo')

print('#################   Video 41: Continue     ##############')

'''El -continue- hace que el ciclo For siga interando mientras la condicion sea TRUE,
en el momento de que esta es FALSE el programa salta al siguiente codigo ,en este caso 
a la linea Print ..'''

'''El programa realiza lo siguiente ,continua mientras la condicion se CUMPLA'''

print('-Estos valores son PAR:')
for i in range(6):
    if i % 2 != 0:
      continue
    print(f'\t{i}')

password = 1234
cont = 3
desploqueo=['mia','tony',1989]
p = 0
while p == 3 or cont ==3:
 while cont > 0:
        ing_pass = int(input('Ingresa tu contraseña: '))
        if ing_pass == password:
                    print(f'\tBinevenido Marce ¿Que deseas hacer?..')
                    break
        else:
                print(f'''\t* La contraseña ingresada es Incorrecta.. *
                    \t Te quedan {cont-1} intentos          
                    ''')
                cont -= 1
 else:
    print('\tTu usuario fue bloqueado.. !')
    ing_des = str(input('¿Deseas desbloquear tu Usuario (y/n)?: '))
    if ing_des == 'y':

        ing_list=input('¿Cual es el nombre de tu mascota?: ')
        for ing_list in desploqueo:
            if ing_list == desploqueo:
                p += 1

        ing_list1 = input('¿Cual es el nombre de tu Ahijada?: ')
        for ing_list1 in desploqueo:
            if ing_list1 == desploqueo:
                p += 1

        ing_list2 = int(input('¿En que año naciste?: '))
        for ing_list2 in desploqueo:
            if ing_list2 == desploqueo:
                p += 1

        if p == 3:
            print(f'\tUsuario desploqueado')

    else:
     print('\t Fin del Programa..')
else:
    pass
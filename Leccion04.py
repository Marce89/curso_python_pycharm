print('#########################     video 25    ######################\n')



a = False
b = True


'''El Operador logico AND devuelve True *siempre y cuando* que ambos operando sean True,
 de lo contrario devolvera siempre False'''

resultado = a and b
print(resultado)

'''El operador logico OR devuelve True *si al menos UNO* de los operando es True,
Solo devolvera False cuando ambos sean False'''

resultado = a or b
print(resultado)

'''El operador logico NOT pregunta si algo *NO ES TRUE* ;devolviendo False si el <operando>(a) es True y 
True si el <operando>(a)  es False'''

resultado = not a
print(resultado)

print('#########################     Ejercicio AND:  video 26    ######################\n')

valor = int(input('Escribe un valor: '))
#Si la entrada es True entonces se ejecuta IF,
# en cambio si la esta es False se ejecutara Else

if valor >= 0 and valor <= 5:
    print(f'Ingresaste el numero {valor} esta dentro del rango')
else:
    print(f'Ingresate {valor} esta fuera de rango')



print('#########################     Ejercicio OR:  video 27    ######################\n')

dialibre = True
vacaciones = False

'''El oerador logico OR evalua si al menos una de las expresiones es True se ejecuta el IF,
de lo contrario si y solo si ambas expresiones son False se ejecutara el ELSE'''

if dialibre or vacaciones:
    print(f'Puede ir al Partido del Hijo')
else:
    print(f'Tiene deberes por hacer ')

print('#########################     Ejercicio NOT:  video 28    ######################\n')



dialibre = True
vacaciones = False

'''El operador logico NOT lo que hace es invertir la logica,Si algo NO ES ejecuta el IF,
de lo contrario ejecutara el ELSE'''

if not (dialibre or vacaciones):
    print(f'Tiene deberes por hacer ')
else:
    print(f'Puede ir al Partido del Hijo')


print('#########################     Ejercicio Rango entre 20 y 30   ######################\n')

edad = int(input('Ingresa tu edad: '))

veintes = edad >= 20 and edad < 30
treintas = edad >= 30 and edad < 40

if veintes or treintas:
    #print('Estas dentro del rango (20\'s) y los (30\'s)')
    if veintes:
        print('Esta dentro de los 20\'s')
    elif treintas:
        print('Esta dentro de los 30\'s')

else:
    print("No esta entre los (20's) y los (30's)\n ")


print("============================================================================================")
print('Ejercicio : Solicitar al usuario dos valores, y determinar cual número es el mayor\n')

numero1 = int(input('Ingresa un numero: '))
numero2 = int(input('Ahora ingresa otro numero: '))

if numero1 > numero2:
    print(f'\n\t* El {numero1} es el Mayor')
elif numero1 < numero2:
    print(f'\n\t* El {numero2} es el Mayor')
else:
    print("\n\t* Son IGUALES")



print('#########################     Ejercicio: video 29    ######################\n')

mensaje= str

libro = input('Proporciona el nombre del Libro: ')
id = int(input('Proporciona el ID: '))
precio = float(input('Proporciona su precio: '))
envio = input('¿Tiene envio gratuito? : ')


if envio == 'si':
    envio = True
    mensaje = '-Su pedido Tiene envio gratuito'
elif envio == 'no':
    envio = False
    mensaje = '-Su pedido tiene un costo de envio de $450'
else:
    mensaje = '-Por favor ingresa \'si\' o \'no\' '

print(f'''

\t-Nombre: {libro}'
\t-ID: {id} 
\t-Precio: ${precio} 
\t{mensaje}

''')




'''Python asigna valores booleanos a los valores segun los tipos. 

Para Tipos Numericos INT / FLOAT:
    _____________________________________________________________________________________________
    * Los valores cero (0) son valores False y los valores diferentes de cero son valores True.
    ---------------------------------------------------------------------------------------------
Para los tipos cadenas STR:
    _____________________________________________________________________________________________
    * Las cadenas vacías son False y las no vacías son True.
    ---------------------------------------------------------------------------------------------'''


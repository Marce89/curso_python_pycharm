print('\n############# Video 74 : Encapsulamiento en Python #############\n')

'''
1- Al ingresar una atributo como SEMIPUBLICO (_) estamos definiendo que el mismo no debe ser accedido fuera de la CLASE
2- Sin bien Python permite ingresar al atributo fuera de la CLASE no es de buena Practica, para ello se debera crear los Metodos GET y SET
'''

class Persona:
    def __init__(self, nombre, apellido, edad):
        self.nombre = nombre           # Atributo Publico     (sin guiones)
        self._apellido = apellido      # Atributo SemiPublico  (_)
        self.__edad = edad             # Atributo PRIVADO      (__)

    def mostrarDatos(self):
        print(f'Nombre: {self.nombre}\nApellido: {self._apellido}\nEdad: {self.__edad} años')

persona1 = Persona('Marcos', 'Rodriguez', 27)
#persona1._nombre = 'Benjamin'      /Si bien podemos acceder al atrbuto nombre NO es lo reconmendable a la hora de Pogramar SOLO debe accederse dentro de la CLASE
persona1.mostrarDatos()
# persona1.__edad        /No podre acceder al ATRIBUTO edad de manera directa ya que es PRIVADO,solo a traves del METODO
print('\n############# Video 75 : Metodos GET y SET en Python #############\n')

'''
GET = obtener/recuperar
SET = coloca/modificar
'''
class Persona1:
    def __init__(self, nombre, apellido, edad):
        self.nombre = nombre
        self._apellido = apellido
        self.__edad = edad

    def mostrarDatos(self):
        print(f'Nombre: {self.nombre}\nApellido: {self._apellido}\nEdad: {self.__edad} años')
    '''
    ^ El decorador @property nos permite modificar el comportamiento del Metodo para que podamos acceder a los
    atributos como si fuera atributo de nuestra clase o sea que lo llamaremos al metodo SIN ()
    ^ Este decorador ENCAPSULA todos los atributos SEMIPUBLICOS(_) y PRIVADOS (__) demanera que solo podremos acceder a traves
    del METODO
    ^
    '''
    @property
    def metodo_get_apellido(self):
        print('\t*** Accediendo al Metodo GET ***')
        return self._apellido   # Accedemos de manera Indirecta al atributo que esta definido en nuestra CLASE
    '''
        ^ Este decorador @<nombre_del_metodo_get>.setter nos permitira MODIFICAR un atributo Privado o SemiPublico de manera Indirecta con el METODO SET
    '''
    @metodo_get_apellido.setter                     # 1- Aca es importante colocar el mismo Nombre del metodo GET para acceder a/los Atributos que deseamos modificar
    def metodo_set_apellido(self, par_apellido):    # 2- Indicamos el parametro para recibir el VALOR del atributo que vamos a modificar
        self._apellido = par_apellido
        print('\t*** Accediendo al Metodo SET ***')

    @property
    def metodo_get_edad(self):
        return self.__edad

    @metodo_get_edad.setter
    def set_edad(self, paredad):
        self.__edad = paredad

persona2 = Persona1('Julian', 'Valderrama', 25)
persona2.metodo_set_apellido = 'Schweinsteiger'      # 3- Modificamos de manera Indirecta al atributo definido en la CLASE
print(persona2.metodo_get_apellido)                  # Llamamos al METODO sin el ()


print(f'\n\tIngreso al metodo GET y muestro la edad de {persona2.metodo_get_edad} años de manera Indirecta')
persona2.set_edad = 33
print(f'\n\tModifico la edad a {persona2.metodo_get_edad} años en la linea 68 ingresando de manera Indirecta al metodo SET')


print('\n############# Video 76 : Atributos read-only (solo lectura) en Python #############\n')
print('''
> Basicamente seria omitir/borrar el Metodo SET para que no se pueda Modificar los Atributos de la Clase,
de esta manera nos aseguramos que los atributos sean solo de LECTURA.

> Aclarando nuevamente que NO es buena pratica accediendo de manera DIRECTA a los atributos Privados y SemiPublicos
''')

print('\n############# Video 77 : Encapsulando todos los atributos de una Clase #############\n')

class Moto:
    def __init__(self, marca, modelo, cc):
        self.__marca = marca
        self.__modelo = modelo
        self.__cc = cc

    @property
    def marca(self):
        return self.__marca

    @marca.setter
    def set_marca(self, parametro_marca):
        self.__marca = parametro_marca

    @property
    def modelo(self):
        return self.__modelo

    @modelo.setter
    def set_modelo(self, parametro_modelo):
        self.__modelo = parametro_modelo

    @property
    def cilindrada(self):
        return self.__cc

    @cilindrada.setter
    def set_cilindrada(self, parametro_cc):
        self.__cc = parametro_cc

    def mostrar_detalle(self):
        print(f'\tMarca: {self.marca}\n\tModelo: {self.modelo}\n\tCilindrada: {self.cilindrada} cc')


moto = Moto('Kawasaki','Ninja',600)
moto.mostrar_detalle()
# Modifico cada uno de los Atributos accediendo al Metodo SET de cada uno de ellos
moto.set_marca = 'Yamaha'
moto.set_modelo = 'R1'
moto.set_cilindrada = 900
print('=========================')
# Imprimo los atributos YA modificados
moto.mostrar_detalle()





class Curso:
    def __init__(self ):
        self.listamateria = ['Probabilidad','Algebra 1','Fisica']

    def recorrerLista(self):
        print('\nMaterias disponibles\n')
        for index,h in enumerate(self.listamateria):
            print(index+1,'-',h)

if __name__ == '__main__':
    m = Curso()
    m.recorrerLista()


print('\n############# Video 78: Uso de Modulos y Clases en Python #############\n')

'''
1- La forma de Trabajar con Modulos (archivos) en python es,primero ingresamos DESDE DONDE importamos (nombre de archivo) y 
luego indicamos el nombre de la/las CLASE/S con las que deseamos llamar.   

2- En el caso que necesitemos trabajar con todas las CLASES de nuestro archivo, despues del import ingresamos un * (asterisco)
'''

# Indicamos el nombre del archivo y las Clases que utilizara el nuevo Modulo
from Vehiculos import Camiones, Deportivos

# Indicamos el nombre del archivo e Importamos TODAS las Clases del archivo
# from Vehiculos import *


obj_camion1 = Camiones('Scania', 'Top Line 113', 6542)
obj_camion1.mostrar_datos()

obj_camion2 = Camiones('Mercedez Benz', '1114', 4300)
obj_camion2.mostrar_datos()

obj_deportivo = Deportivos('Audi', 'R8', 480, 310)
obj_deportivo.mostrar_datos()


print(f'''\nEl {__name__} nos indica que este es el archivo (Modulo) que se esta ejecutando\n''')


print(' Video 80: Destructor de Objetos en Python '.center(90,'#'))

'''
La creacion del metodo destructor __del__ dentro de nuestra clase nos puede servir para liberar recursos
que necesitemos liberar,si bien NO es comun utilizarlo    
'''

print('Creacion de Objetos'.center(30,'-'))
obj_deportivo = Deportivos('Ferrari', 'Spider', 360, 270)
obj_deportivo.mostrar_datos()
print(' Eliminacion de Objetos '.center(30,'-'))
del obj_deportivo   # Con la palabra reservada < del > estamos eliminando de manera directa el objeto,si lo imprimimos no deberia mostrarse
#print(obj_deportivo.mostrar_datos())
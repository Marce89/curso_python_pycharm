from figurageometrica import FiguraGeometrica
from color import Color

'''
> La sintaxis --super().__init__(lado)-- NO es recomendable utilizarla cuando trabajamos con herencia 
  multiple ya que al intentar inicializar los atributos y/o metodos de las clases PADRE, no sabriamos  
  de cual es la clase que se esta mandando a llamar.Python entiende que hay 2 clases Padres por ende 
  podemos ingresar los atributos <lado> como <color> (Descomentar linea 13 y jugar con los atributos para entenderlo)
  
> Linea 16: Para un cuadrado el ancho y el alto son los mismo ,por ende lo guardamos en una misma variable <lado>

> IMPORTATNTE: Se debe tener en cuenta que NO podemos realizar operaciones aritmeticas con cadenas, para ello debemos
 convertir los atributos en INT o FLOAT; Tal como sucede en la Linea 27
 Esto se debe a que los atributos <self.__ancho> y <self.__alto> estan ENCAPSULADOS en los metodos respectivos y 
 estos devuelven valores tipo STR. 
'''


class Cuadrado(FiguraGeometrica, Color):
    def __init__(self, lado, color):
        # super().__init__()
        FiguraGeometrica.__init__(self, lado, lado)   # En este caso SI debemos pasar el parametro de SELF
        Color.__init__(self, color)

    def calcular_area(self):
        return int(self.get_ancho) * int(self.get_alto)

    def __str__(self):
        return f'{FiguraGeometrica.__str__(self)} {Color.__str__(self)}'

class Color:
    def __init__(self, color):
        self.__color = color

    @property
    def get_color(self):
        return f'{self.__color}'

    @get_color.setter
    def set_color(self, col):
        self.__color = col

    def __str__(self):
        return f' Color[color: {self.get_color}]'
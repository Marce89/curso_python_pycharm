from cuadrado import Cuadrado
from rectangulo import Rectangulo

cuadrado1 = Cuadrado(2, 'Rojo')
print(f'Ancho: {cuadrado1.get_ancho}')
print(f'Lado: {cuadrado1.get_alto}')
print(f'Color: {cuadrado1.get_color}')

print('\n')
print(' Video 87: Metodo MRO: Method Resolution Order en Python '.center(70,'#'))

'''
Este metodo NOS permite saber el Orden con el que se manda a llamar nuestras clases PADRES
'''

print(Cuadrado.mro())
print('\n')
print(' Video 88: Laboratorio Figura Geometrica '.center(70,'#'))
'''Al imprimir nuestro objeto,indirectamente vamos a estar retornado los metodos STR de nuestra clases PADRES
 para el caso imprime al metodo str de <Cuadrado> que este a su vez hereda los metodos str de las clases padres <FiguraGeometrica> y <Color>
'''

print(' Se genera el objeto Cuadrado '.center(50,'*'))
cuadrado2 = Cuadrado(lado=5, color='naranja')
cuadrado2.set_alto = -8
print(f'Area del Cuadrado = {cuadrado2.calcular_area()} m2')
print(cuadrado2)

print('\n')
print(' Se genera el objeto Rectangulo '.center(50,'*'))
rectangulo1 = Rectangulo(ancho= 6,alto= 7,color= 'verde')
print(f'Area del Rectangulo = {rectangulo1.calcular_area()} m2')
print(rectangulo1)

if __name__ == '__main__':
    print(' Video 84,85 y 86: Herencia Multiple en python '.center(70, '#'))


class FiguraGeometrica:
    def __init__(self, ancho, alto):
        if self._validar_valor(ancho):  # valida que el valor ingresado este dentro del rango del METODO
            self.__ancho = ancho
        else:
            self.__ancho = 0
            print(f'Ingresaste {ancho} mts. de ancho\n> valor fuera de Rango <')
        if self._validar_valor(alto):
            self.__alto = alto
        else:
            self.__alto = 0
            print(f'Ingresaste {alto} mts. de alto \n> valor fuera de Rango <')

    @property
    def get_ancho(self):
        return f'{self.__ancho}'

    @get_ancho.setter
    def set_ancho(self, anc):
        if self._validar_valor(anc):
            self.__ancho == anc
        else:
            self.__ancho = 0
            print(f'Ingresaste {anc} mts. de ancho \n> valor fuera de Rango <')

    @property
    def get_alto(self):
        return f'{self.__alto}'

    @get_alto.setter
    def set_alto(self, alt):
        if self._validar_valor(alt):
            self.__alto = alt
        else:
            self.__alto = 0
            print(f'Ingresaste {alt} mts. de alto \n> valor fuera de Rango <')

    def __str__(self):
        return f'FiguraGeometrica [Ancho: {self.get_ancho} - Alto: {self.get_alto}]'

    def _validar_valor(self, valor):  # El _ o __ indica que este METODO solo debe usarse dentro de esta CLASE (FiguraGeometrica)
        return True if 0 < valor <= 10 else False

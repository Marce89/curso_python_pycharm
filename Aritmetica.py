print('\n############# Video 67 y 68: Ejercicio Clase Aritmetica I y II #############\n')


class Aritmetica:
    '''
    Clase Aritmetica para realizar las operaciones de sumar,restar, etc
    '''
    def __init__(self ,operandoA ,operandoB):
        self.operandoa = operandoA
        self.operandob = operandoB

    def sumar(self):
        return  self.operandoa+self.operandob
    def restar(self):
        return  self.operandoa-self.operandob
    def dividir(self):
        return  self.operandoa/self.operandob
    def multiplicar(self):
        return  self.operandoa*self.operandob

    def mostrar_datos(self):
        print(f'''
        \nResultado: 
        {self.operandoa} + {self.operandob} es: {self.sumar()}
        {self.operandoa} - {self.operandob} es: {self.restar()}
        {self.operandoa} * {self.operandob} es: {self.multiplicar()}
        {self.operandoa} / {self.operandob} es: {self.dividir():.3f} 
        ''') # Con "  :.numerof "  estamos indicando la cant de decimales que queremos mostrar (line 27)

valor1 = int(input('Ingresa un numero: '))
valor2 = int(input('Ahora ingresa otro: '))
obj=Aritmetica(valor1,valor2)
obj.mostrar_datos()
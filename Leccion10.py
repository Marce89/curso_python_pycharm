print('#################   Video 45 y 46: Colecciones Tuplas en Python    ##############')
'''Las tuplas funciona casi similar a las listas con la diferencia de que estas
# son inmutables o sea no se pueden eliminar,modificar o agregar elementos a una TUPLA'''

# Una tupla queda definida entre PARENTESIS
frutas = ('naranja', 'manazana', 'palta', 'pera', 'anana', 'uvas')
print(frutas)
# Podemos saber la Cantidad de Elemnetos de la TUPLA
print('\n** Esta Tupla contiene', len(frutas), 'elementos **')
# Podemos acceder a un indice de la TUPLA
print('\n', frutas[4])
# Podemos acceder a un RANGO
print('\n', frutas[2:4])  # Sin incluir el ultimo elemento
# Recorrer una TUPLA y que se Imprima los elementos uno al lado de otro con guion
for fruta in frutas:
    print(fruta, end='-')

'''En el caso que se necesite modificar un/los elementos de una TUPLA ,debemos convertirla a una LISTA
realizar la modificacion ,y luego convertir la lista a una tupla nuevamente'''

listafruta = list(frutas)
listafruta[0] = 'SANDIA'
frutas = tuple(listafruta)
print('\n\n', frutas)

# Solo se podra eliminar una TUPLA por completo
del frutas
print(frutas)

print('#################   Ejercicios de Tuplas y Listas    ##############')

'''Dada la siguiente tupla, crear una lista que sólo incluya los números menor 
que 5 utilizando un ciclo for: tupla = (13, 1, 8, 3, 2, 5, 8)'''

tupla = (13, 1, 8, 3, 2, 5, 8)
listamenor = []       # Asi declaramos una lista VACIA
listamayor = []

for i in tupla:
    if i < 5:
        listamenor.append(i)
    else:
        listamayor.append(i)
print(f'Lista Menor a 5 : {listamenor}')
print(f'Lista Mayor a 5 : {listamayor}')

print('\n#################   Video 47: Colecciones Set en python     ##############')

'''
* La particularidad que un SET no mantiene un orden de almacenamiento
* No puede eliminar un elemento de un SET
* Solo se puede agregar o eliminar elementos
* No soporta elemntos duplicados
'''
set_planetas = {'Urano', 'Venus', 'Marte', 'Saturno', 'Mercurio'}

print(set_planetas)
print(len(set_planetas))
print(set_planetas[3])   # No se puede ingresar a un elemento de un SET a travez de su INNDEX

# Revisisar si un elemento pertenece al SET
print('Marte' and 'Urano' and 'Venus' in set_planetas)

# Agregar elementos a nuestro SET
set_planetas.add('Neptuno')
print(set_planetas)

# Los elementos del SET son Unicos ,en este caso agregamos nuevamente el elemento -Marte- y uno NUEVO -TIERRA-
# Solo se mostraran los elementos que NO esten duplicados en el set
set_planetas.add('Marte' and 'TIERRA')
print(set_planetas)

# Eliminar un elemento del set con REMOVE
'''La funcion remove arroja un ERROR si el elemento a eliminar NO se encuentra'''
set_planetas.remove('TIERRAS')
print(set_planetas)

# todo Eliminar un elemento del set con DISCARD
'''La funcion discard ,simplemente NO elimina el elemento ya que no lo encuentra'''
set_planetas.discard('TIERRAS')
print(set_planetas)

# todo- ¿ Como Limpiar un set ?

set_planetas.clear()
print(f'El set se encuentra sin elementos --> {set_planetas}')

# todo- Eliminar por completo nuestro SET

del set_planetas
print(set_planetas)

print('\n#################   Video 48: Colecciones Diccionarios en python     ##############')

'''Los diccionarios funcionan igual que los Set ,no poseen indice ;para recuperar un elementos debemos a puntar a la KEY'''

diccionario = {
    'arg': 'Argentina', 'py': 'Paraguay', 'uru': 'Uruguay', 'per': 'Peru'
}

print(diccionario)
# Largo
print(len(diccionario))
# Acceder a un elemento del diccionario (KEY)
print(f"\nAccedo a un elemento a traves de la key --> {diccionario['per']}")
print(f"\nTambien se puede acceder usando el metodo GET --> {diccionario.get('arg')}")

# Modificando elementos
diccionario['uru'] = 'URUGUAY'
print(f'\n{diccionario}')

# Recorrer un dicionario

# todo/ Con la funcion ITEMS podemos regresar la KEY y el VALOR del diccionario

for key, valor in diccionario.items():
    print(f'{key}: es la KEY,del valor: {valor}')

# todo/ Con la funcion KEY podemos regresar solo las KEYS del diccionario

print('Solo regrasa las Key')
for termino in diccionario.keys():
    print(f'\t{termino}')

# todo/ Con la funcion VALUE podemos regresar solo los VALORES del diccionario

print('Solo regrasa los Values')
for termino in diccionario.values():
    print(f'''\t{termino}''')

# Averiguamos si un elemento se encuentra en nuestro diccionario
# Solo se puede saber la KEYS
print('uru' in diccionario)

# Se puede abrebar una nuevo elemento pero no puede estar duplicado
diccionario['col'] = 'Colombia'
print(diccionario)

# Remover un elemento

diccionario.pop('per')
print(diccionario)

# Limpiar todos los elementos sin borrar la Variable

diccionario.clear()
print(diccionario)

# Eliminar un diccionario

del diccionario
#print(diccionario)